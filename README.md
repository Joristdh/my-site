# Personal Homepage

This is a page to showcase my personal skills in frontend development. It's available at [Joristdh.web.app](https://joristdh.web.app)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Compiles and deploys to Firebase Hosting
```
npm run firebase
```
