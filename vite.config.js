import { defineConfig } from 'vite'
import Vue from '@vitejs/plugin-vue2'
import Components from 'unplugin-vue-components/vite';
import { VuetifyResolver } from 'unplugin-vue-components/resolvers';

const path = require("path");
export default defineConfig({
    plugins: [
        Vue({
            template: {
                transformAssetUrls: {
                    'v-img': ['src'],
                    'v-parallax': ['src']
                }
            }
        }),
        Components({
            resolvers: [
                VuetifyResolver()
            ]
        })
    ],
    resolve: {
        extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json', '.vue'],
        alias: {
            "@": path.resolve(__dirname, "./src"),
        },
    }
})
