import App from "@/App";
import Vue from "vue";

import router from '@/routes';
import Vuetify from "vuetify/lib";

Vue.config.productionTip = false;
Vue.use(Vuetify);

// We don't use any FB features
// if (import.meta.env.PROD) {
//     initializeApp({
//         apiKey: "AIzaSyByInRzdnxwAE531Z0afzsagALt0gtINME",
//         authDomain: "joristdh.firebaseapp.com",
//         databaseURL: "https://joristdh.firebaseio.com",
//         projectId: "joristdh",
//         storageBucket: "joristdh.appspot.com",
//         messagingSenderId: "264261379887",
//         appId: "1:264261379887:web:2a8f596eb5ee333424a3da"
//     });
// }

new Vue({
    router,
    vuetify: new Vuetify({
        theme: { // Check if user has a saved preference, use system theme otherwise
            dark: JSON.parse(localStorage.getItem("dark")) ?? matchMedia("(prefers-color-scheme: dark)").matches
        },
        icons: {
            iconfont: 'mdiSvg'
        },
        breakpoint: {
            mobileBreakpoint: "sm"
        }
    }),
    render: h => h(App)
}).$mount("#app");
