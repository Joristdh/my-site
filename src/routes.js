import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)


import Overview from "@/components/Overview"
import Slides from "@/components/Slides"

import Master from "@/content/Master"
import Bachelor from "@/content/Bachelor";
import SEP from "@/content/SEP";
import Atos from "@/content/Atos";
import CIZ from "@/content/CIZ";

export default new VueRouter({
  mode: 'history',
  base: import.meta.env.BASE_URL,
  routes: [
    {
      path: "/", component: Overview
    }, {
      path: "/theory/:i?", component: Slides,
      props: ({ params: { i } }) => ({ content: [Master, Bachelor], i: i })
    }, {
      path: "/practice/:i?", component: Slides,
      props: ({ params: { i } }) => ({ content: [CIZ, Atos, SEP], i: i })
    }, {
      path: "*", redirect: "/"
    }
  ]
})
